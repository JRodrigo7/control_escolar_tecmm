﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class FrmAsignacion : Form
    {
        private AsignacionManejador _asignacionManejador;
        private ProfesorManejador _profesorManejador;
        private MateriaManejador _materiaManejador;
        private GruposManejador _gruposManejador;
        public FrmAsignacion()
        {
            InitializeComponent();
            _asignacionManejador = new AsignacionManejador();
            _profesorManejador = new ProfesorManejador();
            _materiaManejador = new MateriaManejador();
            _gruposManejador = new GruposManejador();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarAsignacion();
                BuscarAsignacion("");
                Limpiar();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea Eliminar el Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Limpiar();
                    BuscarAsignacion("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            
        }

        private void FrmAsignacion_Load(object sender, EventArgs e)
        {
            BuscarDatos("");
            BuscarAsignacion("");
        }
        private void BuscarDatos(string filtro)
        {
            try
            {
                cmbClaveGrupo.DataSource = _gruposManejador.GetGrupos(filtro);
                cmbClaveGrupo.DisplayMember = "ClaveGrupo";
                cmbNControl.DataSource = _profesorManejador.GetProfesors(filtro);
                cmbNControl.DisplayMember = "NControl";
                cmbIDMateria.DataSource = _materiaManejador.GetMaterias(filtro);
                cmbIDMateria.DisplayMember = "IDMateria";
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }
        private void GuardarAsignacion()
        {
            _asignacionManejador.Guardar(new Asignacion
            {
                idA = Convert.ToInt32(lblId.Text),
                fkNControl = cmbNControl.Text,
                fkIDMateria = cmbIDMateria.Text,
                fkClaveGrupo = cmbClaveGrupo.Text
            });
        }
        private void BuscarAsignacion(string filtro)
        {
            dgbAsignacion.DataSource = _asignacionManejador.GetAsignacions(filtro);
        }
        private void Limpiar()
        {
            lblId.Text = "0";
            txtBuscar.Text = "";
            cmbClaveGrupo.Text = "";
            cmbIDMateria.Text = "";
            cmbNControl.Text = "";
        }
        private void Eliminar()
        {
            var idA = dgbAsignacion.CurrentRow.Cells["idA"].Value;
            _asignacionManejador.Eliminar(Convert.ToInt32(idA));
        }
        private void ModificarAsignacion()
        {
            lblId.Text = dgbAsignacion.CurrentRow.Cells["idA"].Value.ToString();
            cmbClaveGrupo.Text= dgbAsignacion.CurrentRow.Cells["fkClaveGrupo"].Value.ToString();
            cmbIDMateria.Text = dgbAsignacion.CurrentRow.Cells["fkIDMateria"].Value.ToString();
            cmbNControl.Text = dgbAsignacion.CurrentRow.Cells["fkNControl"].Value.ToString();
        }

        private void DgbAsignacion_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAsignacion();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAsignacion(txtBuscar.Text);
        }
    }
}
