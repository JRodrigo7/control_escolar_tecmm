﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using Microsoft.Office.Interop.Excel;
using Microsoft.CSharp.RuntimeBinder;

namespace ControlEscolar
{
    public partial class FrmUsuarios : Form
    {
        private UsuarioManejador _usiarioManejador;
        private Usuario _usuario;
        public FrmUsuarios()
        {
            InitializeComponent();
            _usiarioManejador = new UsuarioManejador();
            _usuario = new Usuario();
        }

        private void FrmControlEscolar_Load(object sender, EventArgs e)
        {
            BuscarUsuario("");
            ControlBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BuscarUsuario(string filtro)
        {
            dgbUsuario.DataSource = _usiarioManejador.GetUsuarios(filtro);
        }

        private void ControlBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool Activar)
        {
            txtNombre.Enabled = Activar;
            txtApellidoP.Enabled = Activar;
            txtApellidoM.Enabled = Activar;
            txtContrasenia.Enabled = Activar;
        }

        private void LimpiarCuadros()
        {
            txtNombre.Text = "";
            txtApellidoP.Text = "";
            txtApellidoM.Text = "";
            txtContrasenia.Text = "";
            lblId.Text = "0";
        }

        private void CargarUsuario()
        {
            _usuario.IdUsuario = Convert.ToInt32(lblId.Text);
            _usuario.Nombre = txtNombre.Text;
            _usuario.ApellidoPaterno = txtApellidoP.Text;
            _usuario.ApellidoMaterno = txtApellidoM.Text;
            _usuario.Contrasenia = txtContrasenia.Text;
        }

        private void GuardarUsuario()
        {
            _usiarioManejador.Guardar(_usuario);
        }

        private void EliminarUsuario()
        {
            var idUsuario = dgbUsuario.CurrentRow.Cells["IdUsuario"].Value;
            _usiarioManejador.Eliminar(Convert.ToInt32(idUsuario));
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            ControlarCuadros(true);
            LimpiarCuadros();
        }
        private bool ValidarUsuario()
        {
            var tupla = _usiarioManejador.ValidarUsuario(_usuario);
            var valido = tupla.Item1;
            var mensaje = tupla.Item2;
            if (!valido)
            {
                MessageBox.Show(mensaje, "Error de Validacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            return valido;
        }
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlarCuadros(false);
            CargarUsuario();
            try
            {
                if (ValidarUsuario())
                {
                    GuardarUsuario();
                    LimpiarCuadros();
                    BuscarUsuario("");
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            LimpiarCuadros();
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);

            if (MessageBox.Show("Desea Eliminar este Registro","Eliminar Registro",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                try
                {
                    EliminarUsuario();
                    BuscarUsuario("");
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }

        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarUsuario(txtBuscar.Text);
        }

        private void DgbUsuario_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarUsuario();
                BuscarUsuario("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void ModificarUsuario()
        {
            lblId.Text = dgbUsuario.CurrentRow.Cells["IdUsuario"].Value.ToString();
            txtNombre.Text = dgbUsuario.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidoP.Text = dgbUsuario.CurrentRow.Cells["ApellidoPaterno"].Value.ToString();
            txtApellidoM.Text = dgbUsuario.CurrentRow.Cells["ApellidoMaterno"].Value.ToString();
            txtContrasenia.Text = dgbUsuario.CurrentRow.Cells["Contrasenia"].Value.ToString();
        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            ExportarDataGriedViewExcel(dgbUsuario);
        }
        private void ExportarDataGriedViewExcel(DataGridView grd)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                if (fichero.ShowDialog()==DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo =
                        (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    //Recorremos el DataGriedView rellenando la hoja de trabajo

                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName,
                        Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();
                    MessageBox.Show("Reporte Terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Fallo la creacion del archivo intenta con otro nombre","Error de Creacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
