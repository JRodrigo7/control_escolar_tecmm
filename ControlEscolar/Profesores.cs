﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class Profesores : Form
    {
        private ProfesorManejador _profesorManejador;
        private EstadoCiudadesManejador _estadoCiudadesManejador;
        private string Ciudad = "";
        private string Codigo = "";
        
        public Profesores()
        {
            InitializeComponent();
            _profesorManejador = new ProfesorManejador();
            _estadoCiudadesManejador = new EstadoCiudadesManejador();
            dtpFN.Format = DateTimePickerFormat.Custom;
            dtpFN.CustomFormat = "yyyy-MM-dd";
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            ControlarCuadros(true);
            LimpiarCuadros();
        }
        private void BuscarProfesor(string filtro)
        {
            dgbProfesores.DataSource = _profesorManejador.GetProfesors(filtro);
        }

        private void Profesores_Load(object sender, EventArgs e)
        {
            BuscarProfesor("");
            BuscarEstado("");
            Actualizar();
            Contador();
            ControlBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarProfesor();
                BuscarProfesor("");
                LimpiarCuadros();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            if (MessageBox.Show("Desea Eliminar el Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarProfesores();
                    BuscarProfesor("");
                    LimpiarCuadros();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarProfesor(txtBuscar.Text);
        } 
        private void GuardarProfesor()
        {
            _profesorManejador.Guardar(new Entidad.ControlEscolar.Profesores
            {
                NControl = txtNControl.Text,
                Nombre = txtNombre.Text,
                ApellidoP = txtApellidoP.Text,
                ApellidoM = txtApellidoM.Text,
                Direccion = txtDireccion.Text,
                Estado = cmbEstado.Text,
                Ciudad = cmbCiudad.Text,
                NCedula = Convert.ToInt32(txtNCedula.Text),
                Titulo = txtTitulo.Text,
                FNacimiento = dtpFN.Text
            });
        }
        private void ModificarProfesores()
        {
            txtNControl.Text = dgbProfesores.CurrentRow.Cells["NControl"].Value.ToString();
            txtNombre.Text = dgbProfesores.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidoP.Text = dgbProfesores.CurrentRow.Cells["ApellidoP"].Value.ToString();
            txtApellidoM.Text = dgbProfesores.CurrentRow.Cells["ApellidoM"].Value.ToString();
            txtDireccion.Text = dgbProfesores.CurrentRow.Cells["Direccion"].Value.ToString();
            cmbEstado.Text = dgbProfesores.CurrentRow.Cells["Estado"].Value.ToString();
            cmbCiudad.Text = dgbProfesores.CurrentRow.Cells["Ciudad"].Value.ToString();
            txtNCedula.Text = dgbProfesores.CurrentRow.Cells["NCedula"].Value.ToString();
            txtTitulo.Text = dgbProfesores.CurrentRow.Cells["Titulo"].Value.ToString();
            dtpFN.Text = dgbProfesores.CurrentRow.Cells["FNacimiento"].Value.ToString();
        }
        private void EliminarProfesores()
        {
            var NControl = dgbProfesores.CurrentRow.Cells["NControl"].Value;
            _profesorManejador.Eliminar(NControl.ToString());
        }
        private void BuscarEstado(string filtro)
        {
            cmbEstado.DataSource = _estadoCiudadesManejador.GetEstados(filtro);
            cmbEstado.DisplayMember = "Nombre";
        }
        private void BuscarCiudad(string filtro)
        {
            cmbCiudad.DataSource = _estadoCiudadesManejador.GetCiudades(filtro);
            cmbCiudad.DisplayMember = "NombreC";
        }
        private void BuscarCodigo(string filtro)
        {
            cmbCodigo.DataSource = _estadoCiudadesManejador.GetCodigos(filtro);
            cmbCodigo.DisplayMember = "Codigo";
        }
        private void Actualizar()
        {
            lblCodigo.Text = cmbCodigo.Text;
            lblCodigoCiudad.Text = cmbEstado.Text;
            Codigo = lblCodigo.Text;
            Ciudad = lblCodigoCiudad.Text;
            BuscarCodigo(Ciudad);
            BuscarCiudad(Codigo);
        }
        private void ControlBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool Activar)
        {
            txtNombre.Enabled = Activar;
            txtApellidoP.Enabled = Activar;
            txtApellidoM.Enabled = Activar;
            txtDireccion.Enabled = Activar;
            cmbEstado.Enabled = Activar;
            cmbCiudad.Enabled = Activar;
            txtNCedula.Enabled = Activar;
            txtTitulo.Enabled = Activar;
            dtpFN.Enabled = Activar;
        }
        private void CmbEstado_SelectedIndexChanged(object sender, EventArgs e)
        {
            Actualizar();
        }
        private void CmbCodigo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void DgbProfesores_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarProfesores();
                BuscarProfesor("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void LimpiarCuadros()
        {
            txtNControl.Text = "";
            txtNombre.Text = "";
            txtApellidoP.Text = "";
            txtApellidoM.Text = "";
            txtDireccion.Text = "";
            cmbEstado.Text = "";
            cmbCiudad.Text = "";
            txtNCedula.Text = "";
            txtTitulo.Text = "";
            dtpFN.Text = "";
        }
        public void Contador()
        {
            try
            {
                string Anio = dateTimePicker1.Value.ToString("yyyy");
                string Maximo = (from DataGridViewRow row in dgbProfesores.Rows
                                 where row.Cells[0].FormattedValue.ToString().Contains(Anio)
                                 select (row.Cells[0].FormattedValue)).Max().ToString().Substring(6);
                int Max = int.Parse(Maximo) + 1;
                if (int.Parse(Maximo) < 10)
                {
                    txtNControl.Text = "D" + dateTimePicker1.Value.ToString("yyyy") + "0" + Max.ToString();
                }
                else
                {
                    txtNControl.Text = "D" + dateTimePicker1.Value.ToString("yyyy") + (int.Parse(Maximo) + 1).ToString();
                }
            }
            catch(Exception)
            {
                txtNControl.Text = "D" + dateTimePicker1.Value.ToString("yyyy") + "1";
            }
           
        }

        private void TxtNControl_TextChanged(object sender, EventArgs e)
        {

        }

        private void DateTimePicker1_ValueChanged(object sender, EventArgs e)
        {
            Contador();
        }
    }
}
