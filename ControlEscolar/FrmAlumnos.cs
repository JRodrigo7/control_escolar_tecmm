﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;


namespace ControlEscolar
{
    public partial class FrmAlumnos : Form
    {
        private AlumnoManejador _alumnoManejador;
        private EstadoCiudadesManejador _estadoCiudadesManejador;
        private string Ciudad = "";
        private string Codigo = "";
        public FrmAlumnos()
        {
            InitializeComponent();
            _alumnoManejador = new AlumnoManejador();
            _estadoCiudadesManejador = new EstadoCiudadesManejador();
            dtpFN.Format = DateTimePickerFormat.Custom;
            dtpFN.CustomFormat = "yyyy-MM-dd";
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            ControlarCuadros(true);
            LimpiarCuadros();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarAlumno();
                LimpiarCuadros();
                BuscarAlumno("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void CmbEstado_TextUpdate(object sender, EventArgs e)
        {

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            ControlBotones(true, false, false, true);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea Eliminar el Registro?","Eliminar Registro",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                try
                {
                    EliminarAlumno();
                    BuscarAlumno("");
                    LimpiarCuadros();
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
               
            }
        }
        private void DgbAlumno_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarAlumno();
                BuscarAlumno("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void FrmAlumnos_Load(object sender, EventArgs e)
        {
            BuscarAlumno("");
            BuscarEstado("");
            BuscarCodigo("");
            Actualizar();
            ControlBotones(true, false, false, true);
            ControlarCuadros(false);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarAlumno(txtBuscar.Text);
        }
        private void BuscarAlumno(string filtro)
        {
            dgbAlumno.DataSource = _alumnoManejador.GetAlumnos(filtro);
        }
        private void GuardarAlumno()
        {
            _alumnoManejador.Guardar(new Alumno
            {
                NumeroC = Convert.ToInt32(lblNc.Text),
                Nombre = txtNombre.Text,
                ApellidoP = txtApellidoP.Text,
                ApellidoM = txtApellidoM.Text,
                FechaN = dtpFN.Text,
                Domicilio = txtDomicilio.Text,
                CorreoE = txtCorreoE.Text,
                Sexo = txtSexo.Text,
                Estado= cmbEstado.Text,
                Ciudad= cmbCiudad.Text
            });
        }
        private void EliminarAlumno()
        {
            var NumeroC = dgbAlumno.CurrentRow.Cells["NumeroC"].Value;
            _alumnoManejador.Eliminar(Convert.ToInt32(NumeroC));
        }
        private void ModificarAlumno()
        {
            lblNc.Text = dgbAlumno.CurrentRow.Cells["NumeroC"].Value.ToString();
            txtNombre.Text = dgbAlumno.CurrentRow.Cells["Nombre"].Value.ToString();
            txtApellidoP.Text = dgbAlumno.CurrentRow.Cells["ApellidoP"].Value.ToString();
            txtApellidoM.Text = dgbAlumno.CurrentRow.Cells["ApellidoM"].Value.ToString();
            dtpFN.Text = dgbAlumno.CurrentRow.Cells["FechaN"].Value.ToString();
            txtDomicilio.Text = dgbAlumno.CurrentRow.Cells["Domicilio"].Value.ToString();
            txtCorreoE.Text = dgbAlumno.CurrentRow.Cells["CorreoE"].Value.ToString();
            txtSexo.Text = dgbAlumno.CurrentRow.Cells["Sexo"].Value.ToString();
            cmbEstado.Text = dgbAlumno.CurrentRow.Cells["Estado"].Value.ToString();
            cmbCiudad.Text = dgbAlumno.CurrentRow.Cells["Ciudad"].Value.ToString();
        }
        private void LimpiarCuadros()
        {
            txtBuscar.Text = "";
            txtNombre.Text = "";
            txtApellidoP.Text = "";
            txtApellidoM.Text = "";
            dtpFN.Text = "";
            txtDomicilio.Text = "";
            txtCorreoE.Text = "";
            txtSexo.Text = "";
        }
        private void ControlBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool Activar)
        {
            txtBuscar.Enabled = Activar;
            txtNombre.Enabled = Activar;
            txtApellidoP.Enabled = Activar;
            txtApellidoM.Enabled = Activar;
            dtpFN.Enabled = Activar;
            txtDomicilio.Enabled = Activar;
            txtCorreoE.Enabled = Activar;
            txtSexo.Enabled = Activar;
            cmbEstado.Enabled = Activar;
            cmbCiudad.Enabled = Activar;
        }

        private void BuscarEstado(string filtro)
        {
            cmbEstado.DataSource = _estadoCiudadesManejador.GetEstados(filtro);
            cmbEstado.DisplayMember = "Nombre";
        }
        private void BuscarCiudad(string filtro)
        {
            cmbCiudad.DataSource = _estadoCiudadesManejador.GetCiudades(filtro);
            cmbCiudad.DisplayMember = "NombreC";
        }
        private void BuscarCodigo(string filtro)
        {
            cmbCodigo.DataSource = _estadoCiudadesManejador.GetCodigos(filtro);
            cmbCodigo.DisplayMember = "Codigo";
        }
        private void Actualizar()
        {
            lblCodigo.Text = cmbCodigo.Text;
            lblCodigoCiudad.Text = cmbEstado.Text;
            Codigo = lblCodigo.Text;
            Ciudad = lblCodigoCiudad.Text;
            BuscarCodigo(Ciudad);
            BuscarCiudad(Codigo);
        }
        
        private void CmbCodigo_SelectedIndexChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void CmbEstado_TextChanged(object sender, EventArgs e)
        {
            Actualizar();
        }

        private void CmbEstado_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            Actualizar();
        }
    }
}
