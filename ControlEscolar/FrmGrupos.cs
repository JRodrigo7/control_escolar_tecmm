﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;
namespace ControlEscolar
{
    public partial class FrmGrupos : Form
    {
        private GruposManejador _gruposManejador;
        private AlumnoManejador _alumnoManejador;
        private DataSet ds;
        public FrmGrupos()
        {
            InitializeComponent();
            _gruposManejador = new GruposManejador();
            _alumnoManejador = new AlumnoManejador();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            try
            {

            }
            catch (Exception)
            {

                throw;
            }
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarGrupo();
                Limpiar();
                Buscar("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea Eliminar el Registro?","Eliminar Registro",MessageBoxButtons.YesNo)==DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Limpiar();
                    Buscar("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
        }
        private void MostrarNC(string filtro)
        {
                cmbNControl.DataSource = _alumnoManejador.GetAlumnos(filtro);
                cmbNControl.DisplayMember = "NumeroC";
        }

        private void FrmGrupos_Load(object sender, EventArgs e)
        {
            try
            {
                MostrarNC("");
                Buscar("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
           
        }

        private void CmbNControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            
        }
        private void GuardarGrupo()
        {
            _gruposManejador.Guardar(new Grupos
            {
                idGrupo = Convert.ToInt32(lblId.Text),
                NumControl = Convert.ToInt32(cmbNControl.Text),
                Grupo = txtEspecialidad.Text,
                Semestre = txtSemestre.Text,
                Turno = cmbTurno.Text,
                ClaveGrupo=txtClaveGrupo.Text
            });
        }
        private void Limpiar()
        {
            lblId.Text = "0";
            txtBuscar.Text = "";
            cmbNControl.Text = "";
            txtEspecialidad.Text = "";
            txtSemestre.Text = "";
            cmbTurno.Text = "";
            txtClaveGrupo.Text = "";

        }
        private void Modificar()
        {
            lblId.Text = dgbGrupo.CurrentRow.Cells["idGrupo"].Value.ToString();
            cmbNControl.Text = dgbGrupo.CurrentRow.Cells["NumControl"].Value.ToString();
            txtEspecialidad.Text = dgbGrupo.CurrentRow.Cells["Grupo"].Value.ToString();
            txtSemestre.Text = dgbGrupo.CurrentRow.Cells["Semestre"].Value.ToString();
            cmbTurno.Text = dgbGrupo.CurrentRow.Cells["Turno"].Value.ToString();
            txtClaveGrupo.Text = dgbGrupo.CurrentRow.Cells["ClaveGrupo"].Value.ToString();
        }
        private void Eliminar()
        {
            var idGrupo = dgbGrupo.CurrentRow.Cells["idGrupo"].Value;
            _gruposManejador.Eliminar(Convert.ToInt32(idGrupo));
        }

        private void DgbGrupo_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                Modificar();
                Buscar("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void Buscar(string filtro)
        {
            dgbGrupo.DataSource = _gruposManejador.GetGrupos(filtro);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            Buscar(txtBuscar.Text);
        }
    }
}
