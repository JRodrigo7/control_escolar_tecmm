﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class FrmEstudios : Form
    {
        private EstudiosManejador _estudiosManejador;
        private OpenFileDialog _archivo;
        private string _ruta;
        public FrmEstudios()
        {
            InitializeComponent();
            _estudiosManejador = new EstudiosManejador();
            _archivo = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Documento\\";
        }

        private void DgbEstudios_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarEstudios();
                BuscarEstudio("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void FrmEstudios_Load(object sender, EventArgs e)
        {
            BuscarProfesores("");
            BuscarEstudio("");
            ControlBotones(true, false, false, true);
            ControlarCuadros(false);
            LimpiarCuadros();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            ControlarCuadros(true);
            LimpiarCuadros();
        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            GuardarArchivo();
            GuardarEstudio();
            ControlBotones(true, false, false, true);
            ControlarCuadros(false);
            try
            {
                GuardarEstudio();
                BuscarEstudio("");
                LimpiarCuadros();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {

            ControlBotones(true, false, false, true);
            LimpiarCuadros();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            ControlBotones(false, true, true, false);
            if (MessageBox.Show("Desea Eliminar el Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    EliminarEstudio();
                    BuscarEstudio("");
                    LimpiarCuadros();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }

            }
        }
        private void GuardarEstudio()
        {
            _estudiosManejador.Guardar(new Estudios
            {
                NEstudios = Convert.ToInt32(lblNc.Text),
                Estudio = txtEstudio.Text,
                Documentos = txtImagenJPG.Text,
                fkProfesor = cmbProfesores.Text
            });
        }
        private void ModificarEstudios()
        {
            lblNc.Text = dgbEstudios.CurrentRow.Cells["NEstudios"].Value.ToString();
            txtEstudio.Text = dgbEstudios.CurrentRow.Cells["Estudio"].Value.ToString();
            txtImagenJPG.Text = dgbEstudios.CurrentRow.Cells["Documentos"].Value.ToString();
            cmbProfesores.Text = dgbEstudios.CurrentRow.Cells["fkProfesor"].Value.ToString();
        }
        private void EliminarEstudio()
        {
            var Nestudio = dgbEstudios.CurrentRow.Cells["NEstudios"].Value;
            _estudiosManejador.Eliminar(Convert.ToInt32(Nestudio));
        }
        private void BuscarEstudio(string filtro)
        {
            dgbEstudios.DataSource = _estudiosManejador.GetEstudios(filtro);
        }
        private void BuscarProfesores(string filtro)
        {
            cmbProfesores.DataSource = _estudiosManejador.GetProfesores(filtro);
            cmbProfesores.DisplayMember = "NControl";
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarEstudio(txtBuscar.Text);
        }
        private void ControlBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool Activar)
        {
            txtEstudio.Enabled = Activar;
            cmbProfesores.Enabled = Activar;
            txtImagenJPG.Enabled = Activar;
        }
        private void LimpiarCuadros()
        {
            txtEstudio.Text = "";
            cmbProfesores.Text = "";
            txtImagenJPG.Text = "";
            lblNc.Text = "0";
        }
        private void CargarArchivo()
        {
            _archivo.Filter = "Formato Tipo (*.pdf)|*.pdf";
            _archivo.Title = "Cargar Formato";
            _archivo.ShowDialog();
            _=_archivo.ShowDialog();

            if (_archivo.FileName != "")
            {
                var archivo = new FileInfo(_archivo.FileName);

                txtImagenJPG.Text = archivo.Name;
            }
        }
        private void GuardarArchivo()
        {
            if (_archivo.FileName != null)
            {
                if (_archivo.FileName != "")
                {
                    var archivo = new FileInfo(_archivo.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");
                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }
            }
        }
        private void EliminarImagen()
        {
            if (MessageBox.Show("Estas seguro de Eliminar Archivo", "Eliminar Archivo", MessageBoxButtons.YesNo, MessageBoxIcon.Warning) == DialogResult.Yes)
            {
                if (Directory.Exists(_ruta))
                {
                    var obtenerArchivos = Directory.GetFiles(_ruta, "*.pdf");
                    FileInfo archivoAnterior;

                    if (obtenerArchivos.Length != 0)
                    {
                        archivoAnterior = new FileInfo(obtenerArchivos[0]);
                        if (archivoAnterior.Exists)
                        {
                            archivoAnterior.Delete();
                        }
                    }
                }
            }
            else
            {

            }
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            EliminarImagen();
        }

        private void BtnCargar_Click(object sender, EventArgs e)
        {
            CargarArchivo();
        }
    }
}
