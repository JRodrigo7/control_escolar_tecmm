﻿namespace ControlEscolar
{
    partial class PantallaPrincipal
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PantallaPrincipal));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.catalogosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.usuariosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.alumnosToolStripMenuItem1 = new System.Windows.Forms.ToolStripMenuItem();
            this.profesoresToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.estudiosToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.materiasToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.escuelaToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.msMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.gruposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.asignacionDeGruposToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.capturaCalificacionesToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.catalogosToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(800, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // catalogosToolStripMenuItem
            // 
            this.catalogosToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.usuariosToolStripMenuItem,
            this.alumnosToolStripMenuItem1,
            this.profesoresToolStripMenuItem,
            this.estudiosToolStripMenuItem,
            this.materiasToolStripMenuItem,
            this.escuelaToolStripMenuItem,
            this.gruposToolStripMenuItem,
            this.asignacionDeGruposToolStripMenuItem,
            this.capturaCalificacionesToolStripMenuItem});
            this.catalogosToolStripMenuItem.Name = "catalogosToolStripMenuItem";
            this.catalogosToolStripMenuItem.Size = new System.Drawing.Size(72, 20);
            this.catalogosToolStripMenuItem.Text = "Catalogos";
            this.catalogosToolStripMenuItem.Click += new System.EventHandler(this.CatalogosToolStripMenuItem_Click);
            // 
            // usuariosToolStripMenuItem
            // 
            this.usuariosToolStripMenuItem.Image = ((System.Drawing.Image)(resources.GetObject("usuariosToolStripMenuItem.Image")));
            this.usuariosToolStripMenuItem.Name = "usuariosToolStripMenuItem";
            this.usuariosToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.usuariosToolStripMenuItem.Text = "Usuarios";
            this.usuariosToolStripMenuItem.Click += new System.EventHandler(this.UsuariosToolStripMenuItem_Click);
            // 
            // alumnosToolStripMenuItem1
            // 
            this.alumnosToolStripMenuItem1.Image = global::ControlEscolar.Properties.Resources.icons8_user_50;
            this.alumnosToolStripMenuItem1.Name = "alumnosToolStripMenuItem1";
            this.alumnosToolStripMenuItem1.Size = new System.Drawing.Size(192, 22);
            this.alumnosToolStripMenuItem1.Text = "Alumnos";
            this.alumnosToolStripMenuItem1.Click += new System.EventHandler(this.AlumnosToolStripMenuItem1_Click);
            // 
            // profesoresToolStripMenuItem
            // 
            this.profesoresToolStripMenuItem.Image = global::ControlEscolar.Properties.Resources.icons8_user_50;
            this.profesoresToolStripMenuItem.Name = "profesoresToolStripMenuItem";
            this.profesoresToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.profesoresToolStripMenuItem.Text = "Profesores";
            this.profesoresToolStripMenuItem.Click += new System.EventHandler(this.ProfesoresToolStripMenuItem_Click);
            // 
            // estudiosToolStripMenuItem
            // 
            this.estudiosToolStripMenuItem.Image = global::ControlEscolar.Properties.Resources.icons8_user_50;
            this.estudiosToolStripMenuItem.Name = "estudiosToolStripMenuItem";
            this.estudiosToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.estudiosToolStripMenuItem.Text = "Estudios";
            this.estudiosToolStripMenuItem.Click += new System.EventHandler(this.EstudiosToolStripMenuItem_Click);
            // 
            // materiasToolStripMenuItem
            // 
            this.materiasToolStripMenuItem.Image = global::ControlEscolar.Properties.Resources.icons8_user_50;
            this.materiasToolStripMenuItem.Name = "materiasToolStripMenuItem";
            this.materiasToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.materiasToolStripMenuItem.Text = "Materias";
            this.materiasToolStripMenuItem.Click += new System.EventHandler(this.MateriasToolStripMenuItem_Click);
            // 
            // escuelaToolStripMenuItem
            // 
            this.escuelaToolStripMenuItem.Image = global::ControlEscolar.Properties.Resources.icons8_user_50;
            this.escuelaToolStripMenuItem.Name = "escuelaToolStripMenuItem";
            this.escuelaToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.escuelaToolStripMenuItem.Text = "Escuela";
            this.escuelaToolStripMenuItem.Click += new System.EventHandler(this.EscuelaToolStripMenuItem_Click);
            // 
            // msMenu
            // 
            this.msMenu.Name = "msMenu";
            this.msMenu.Size = new System.Drawing.Size(61, 4);
            // 
            // gruposToolStripMenuItem
            // 
            this.gruposToolStripMenuItem.Image = global::ControlEscolar.Properties.Resources.icons8_user_50;
            this.gruposToolStripMenuItem.Name = "gruposToolStripMenuItem";
            this.gruposToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.gruposToolStripMenuItem.Text = "Grupos";
            this.gruposToolStripMenuItem.Click += new System.EventHandler(this.GruposToolStripMenuItem_Click);
            // 
            // asignacionDeGruposToolStripMenuItem
            // 
            this.asignacionDeGruposToolStripMenuItem.Image = global::ControlEscolar.Properties.Resources.icons8_user_50;
            this.asignacionDeGruposToolStripMenuItem.Name = "asignacionDeGruposToolStripMenuItem";
            this.asignacionDeGruposToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.asignacionDeGruposToolStripMenuItem.Text = "Asignacion de Grupos";
            this.asignacionDeGruposToolStripMenuItem.Click += new System.EventHandler(this.AsignacionDeGruposToolStripMenuItem_Click);
            // 
            // capturaCalificacionesToolStripMenuItem
            // 
            this.capturaCalificacionesToolStripMenuItem.Image = global::ControlEscolar.Properties.Resources.icons8_user_50;
            this.capturaCalificacionesToolStripMenuItem.Name = "capturaCalificacionesToolStripMenuItem";
            this.capturaCalificacionesToolStripMenuItem.Size = new System.Drawing.Size(192, 22);
            this.capturaCalificacionesToolStripMenuItem.Text = "Captura Calificaciones";
            this.capturaCalificacionesToolStripMenuItem.Click += new System.EventHandler(this.CapturaCalificacionesToolStripMenuItem_Click);
            // 
            // PantallaPrincipal
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "PantallaPrincipal";
            this.Text = "PantallaPrincipal";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.PantallaPrincipal_Load);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem catalogosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem usuariosToolStripMenuItem;
        private System.Windows.Forms.ContextMenuStrip msMenu;
        private System.Windows.Forms.ToolStripMenuItem alumnosToolStripMenuItem1;
        private System.Windows.Forms.ToolStripMenuItem profesoresToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem estudiosToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem materiasToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem escuelaToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem gruposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem asignacionDeGruposToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem capturaCalificacionesToolStripMenuItem;
    }
}