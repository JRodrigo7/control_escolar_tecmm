﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace ControlEscolar
{
    public partial class PantallaPrincipal : Form
    {
        public PantallaPrincipal()
        {
            InitializeComponent();
        }

        private void UsuariosToolStripMenuItem_Click(object sender, EventArgs e)
        {
           
            FrmUsuarios usuarios = new FrmUsuarios();
            usuarios.ShowDialog();
        }

        private void PantallaPrincipal_Load(object sender, EventArgs e)
        {

        }

        private void CatalogosToolStripMenuItem_Click(object sender, EventArgs e)
        {

        }

        private void AlumnosToolStripMenuItem1_Click(object sender, EventArgs e)
        {
            FrmAlumnos alumnos = new FrmAlumnos();
            alumnos.ShowDialog();
        }

        private void ProfesoresToolStripMenuItem_Click(object sender, EventArgs e)
        {
            Profesores profesores = new Profesores();
            profesores.ShowDialog();
        }

        private void EstudiosToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEstudios estudios = new FrmEstudios();
            estudios.ShowDialog();
        }

        private void MateriasToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmMaterias materias = new FrmMaterias();
            materias.ShowDialog();
        }

        private void EscuelaToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmEscuela escuela = new FrmEscuela();
            escuela.ShowDialog();
        }

        private void GruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmGrupos grupos = new FrmGrupos();
            grupos.ShowDialog();
        }

        private void AsignacionDeGruposToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmAsignacion asignacion = new FrmAsignacion();
            asignacion.ShowDialog();
        }

        private void CapturaCalificacionesToolStripMenuItem_Click(object sender, EventArgs e)
        {
            FrmCalificaciones calificaciones = new FrmCalificaciones();
            calificaciones.ShowDialog();
        }
    }
}
