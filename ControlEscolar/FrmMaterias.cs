﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class FrmMaterias : Form
    {
        private MateriaManejador _materiaManejador;
        public FrmMaterias()
        {
            InitializeComponent();
            _materiaManejador = new MateriaManejador();
        }

        private void FrmMaterias_Load(object sender, EventArgs e)
        {
            BuscarMaterias("");
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {

        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {

        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {

        }
        public void BuscarMaterias(string filtro)
        {
            dgbMateria.DataSource = _materiaManejador.GetMaterias(filtro);
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            BuscarMaterias(txtBuscar.Text);
        }
        private void ControlBotones(bool Nuevo, bool Guardar, bool Cancelar, bool Eliminar)
        {
            btnNuevo.Enabled = Nuevo;
            btnGuardar.Enabled = Guardar;
            btnCancelar.Enabled = Cancelar;
            btnEliminar.Enabled = Eliminar;
        }
        private void ControlarCuadros(bool Activar)
        {
            txtIdMateria.Enabled = Activar;
            txtNombreMateria.Enabled = Activar;
            txtHorasMateria.Enabled = Activar;
            txtHorasPractica.Enabled = Activar;
        }
    }
}
