﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;
using Microsoft.Office.Interop.Excel;
using Microsoft.CSharp.RuntimeBinder;

namespace ControlEscolar
{
    public partial class FrmCalificaciones : Form
    {
        private CalificacionesManejador _calificacionesManejador;
        private MateriaManejador _materiaManejador;
        private GruposManejador _gruposManejador;
        private AlumnoManejador _alumnoManejador;
        public FrmCalificaciones()
        {
            InitializeComponent();
            _calificacionesManejador = new CalificacionesManejador();
            _materiaManejador = new MateriaManejador();
            _gruposManejador = new GruposManejador();
            _alumnoManejador = new AlumnoManejador();
        }

        private void BtnNuevo_Click(object sender, EventArgs e)
        {

        }

        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarCalificaiones();
                Limpiar();
                BuscarCalificaciones("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }

        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            Limpiar();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea Eliminar el Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    Limpiar();
                    BuscarCalificaciones("");
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }
            }
            
        }

        private void FrmCalificaciones_Load(object sender, EventArgs e)
        {
            try
            {
                BuscarDatos("");
                BuscarCalificaciones("");
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
           
        }

        private void DgbCalificaciones_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                ModificarCalificaciones();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void GuardarCalificaiones()
        {
            _calificacionesManejador.Guardar(
                new Calificaciones
                {
                    idC = Convert.ToInt32(lblId.Text),
                    fkClaveGrupo = cmbClaveGrupo.Text,
                    fkNumeroControl = Convert.ToInt32(cmbNControl.Text),
                    fkIDMateria = cmbIDMateria.Text,
                    Parcial_1 = Convert.ToInt32(txtParcial1.Text),
                    Parcial_2 = Convert.ToInt32(txtParcial2.Text),
                    Parcial_3 = Convert.ToInt32(txtParcial3.Text),
                    Parcial_4 = Convert.ToInt32(txtParcial4.Text)
                });
        }
        private void BuscarCalificaciones(string filtro)
        {
            dgbCalificaciones.DataSource = _calificacionesManejador.GetCalificaciones(filtro);
        }
        private void Limpiar()
        {
            lblId.Text = "0";
            cmbClaveGrupo.Text = "";
            cmbIDMateria.Text = "";
            cmbNControl.Text = "";
            txtParcial1.Text = "";
            txtParcial2.Text = "";
            txtParcial3.Text = "";
            txtParcial4.Text = "";
            txtBuscar.Text = "";
        }
        private void Eliminar()
        {
            var idC = dgbCalificaciones.CurrentRow.Cells["idC"].Value;
            _calificacionesManejador.Eliminar(Convert.ToInt32(idC));
        }
        private void ModificarCalificaciones()
        {
            lblId.Text = dgbCalificaciones.CurrentRow.Cells["idC"].Value.ToString();
            cmbClaveGrupo.Text = dgbCalificaciones.CurrentRow.Cells["fkClaveGrupo"].Value.ToString();
            cmbIDMateria.Text = dgbCalificaciones.CurrentRow.Cells["fkIDMateria"].Value.ToString();
            cmbNControl.Text = dgbCalificaciones.CurrentRow.Cells["fkNumeroControl"].Value.ToString();
            txtParcial1.Text = dgbCalificaciones.CurrentRow.Cells["Parcial_1"].Value.ToString();
            txtParcial2.Text = dgbCalificaciones.CurrentRow.Cells["Parcial_2"].Value.ToString();
            txtParcial3.Text = dgbCalificaciones.CurrentRow.Cells["Parcial_3"].Value.ToString();
            txtParcial4.Text = dgbCalificaciones.CurrentRow.Cells["Parcial_4"].Value.ToString();
        }

        private void TxtBuscar_TextChanged(object sender, EventArgs e)
        {
            try
            {
                BuscarCalificaciones(txtBuscar.Text);
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            
        }
        private void BuscarDatos(string filtro)
        {
            try
            {
                cmbClaveGrupo.DataSource = _gruposManejador.GetGrupos(filtro);
                cmbClaveGrupo.DisplayMember = "ClaveGrupo";
                cmbNControl.DataSource = _alumnoManejador.GetAlumnos(filtro);
                cmbNControl.DisplayMember = "NumeroC";
                cmbIDMateria.DataSource = _materiaManejador.GetMaterias(filtro);
                cmbIDMateria.DisplayMember = "IDMateria";
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }

        }

        private void BtnExportar_Click(object sender, EventArgs e)
        {
            ExportarDataGriedViewExcel(dgbCalificaciones);
        }
        private void ExportarDataGriedViewExcel(DataGridView grd)
        {
            try
            {
                SaveFileDialog fichero = new SaveFileDialog();
                fichero.Filter = "Excel (*.xls)|*.xls";
                if (fichero.ShowDialog() == DialogResult.OK)
                {
                    Microsoft.Office.Interop.Excel.Application aplicacion;
                    Workbook libros_trabajo;
                    Worksheet hoja_trabajo;
                    aplicacion = new Microsoft.Office.Interop.Excel.Application();
                    libros_trabajo = aplicacion.Workbooks.Add();
                    hoja_trabajo =
                        (Microsoft.Office.Interop.Excel.Worksheet)libros_trabajo.Worksheets.get_Item(1);
                    //Recorremos el DataGriedView rellenando la hoja de trabajo

                    for (int i = 0; i < grd.Rows.Count; i++)
                    {
                        for (int j = 0; j < grd.Columns.Count; j++)
                        {
                            hoja_trabajo.Cells[i + 1, j + 1] = grd.Rows[i].Cells[j].Value.ToString();
                        }
                    }
                    libros_trabajo.SaveAs(fichero.FileName,
                        Microsoft.Office.Interop.Excel.XlFileFormat.xlWorkbookNormal);
                    libros_trabajo.Close(true);
                    aplicacion.Quit();
                    MessageBox.Show("Reporte Terminado", "Reporte", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception)
            {

                MessageBox.Show("Fallo la creacion del archivo intenta con otro nombre", "Error de Creacion", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
    }
}
