﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;
using Entidad.ControlEscolar;
using LogicaNegocio.ControlEscolar;

namespace ControlEscolar
{
    public partial class FrmEscuela : Form
    {
        private OpenFileDialog _archivo;
        private DataSet ds;
        private EscuelaManejador _escuelaManejador;
        private string _ruta;
        private string _ruta2;
        public FrmEscuela()
        {
            InitializeComponent();
            _escuelaManejador = new EscuelaManejador();
            _archivo = new OpenFileDialog();
            _ruta = Application.StartupPath + "\\Logo\\";
            _ruta2 = Application.StartupPath + "\\Documentos\\";
            btnNuevo.Enabled = false;
        }
        private void FrmEscuela_Load(object sender, EventArgs e)
        {
            //MostrarEscuela();
            Mostrar();
            pictureBox1.Image = Image.FromFile(_ruta + lblImagen.Text);

        }
        private void GuardarEscuela()
        {
            _escuelaManejador.Guardar(new Escuela
            {
                NombreEscuela = txtNombreEscuela.Text,
                RFC = txtRFC.Text,
                Domicilio = txtDomicilio.Text,
                NumeroTelefono = Convert.ToInt32(txtNumeroTelefono.Text),
                CorreoE = txtCorreoE.Text,
                PaginaWeb = txtPaginaWeb.Text,
                NombreDirector = txtDirectorEscuela.Text,
                LogoEscuela = lblImagen.Text
            });
        }
        private void ModificarEscuela()
        {
            _escuelaManejador.Modificar(new Escuela
            {
                RFC = txtRFC.Text,
                Domicilio = txtDomicilio.Text,
                NumeroTelefono = Convert.ToInt32(txtNumeroTelefono.Text),
                CorreoE = txtCorreoE.Text,
                PaginaWeb = txtPaginaWeb.Text,
                NombreDirector = txtDirectorEscuela.Text,
                LogoEscuela = lblImagen.Text,
                NombreEscuela = txtNombreEscuela.Text
            });
        }
        private void LimpiarCuadros()
        {
            txtNombreEscuela.Text = " ";
            txtRFC.Text = " ";
            txtDomicilio.Text = " ";
            txtNumeroTelefono.Text = " ";
            txtCorreoE.Text = " ";
            txtPaginaWeb.Text = " ";
            txtDirectorEscuela.Text = " ";
            lblImagen.Text = " ";
        }
        private void BtnNuevo_Click(object sender, EventArgs e)
        {
            try
            {
                GuardarEscuela();
                GuardarImagen();
                Mostrar();
                LimpiarCuadros();
                //MostrarImagen();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
            btnNuevo.Enabled = false;
        }   
        private void BtnGuardar_Click(object sender, EventArgs e)
        {
            try
            {
                ModificarEscuela();
                GuardarImagen();
                LimpiarCuadros();
                Mostrar();
                //pictureBox1.Image = Image.FromFile(_ruta + lblImagen.Text);
                //MostrarImagen();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
        private void BtnCancelar_Click(object sender, EventArgs e)
        {
            LimpiarCuadros();
        }
        private void CargarArchivo()
        {
            _archivo.Filter = "Formato Tipo (*.jpg)|*.jpg";
            _archivo.Title = "Cargar Formato";
            _archivo.ShowDialog();

            if (_archivo.FileName != "")
            {
                var archivo = new FileInfo(_archivo.FileName);

                lblImagen.Text = archivo.Name;
                
            }
        }
        private void GuardarImagen()
        {
            if (_archivo.FileName != null)
            {
                if (_archivo.FileName != "")
                {
                    var archivo = new FileInfo(_archivo.FileName);

                    if (Directory.Exists(_ruta))
                    {
                        var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                        FileInfo archivoAnterior;

                        if (obtenerArchivos.Length != 0)
                        {
                            archivoAnterior = new FileInfo(obtenerArchivos[0]);
                            if (archivoAnterior.Exists)
                            {
                                archivoAnterior.Delete();
                                archivo.CopyTo(_ruta + archivo.Name);
                            }

                        }
                        else
                        {
                            archivo.CopyTo(_ruta + archivo.Name);
                        }
                    }
                    else
                    {
                        Directory.CreateDirectory(_ruta);
                        archivo.CopyTo(_ruta + archivo.Name);
                    }
                }
            }
            //pictureBox1.Image = Image.FromFile(_ruta + lblImagen.Text);
        }
        private void EliminarJGP()
        {
            if (Directory.Exists(_ruta))
            {
                var obtenerArchivos = Directory.GetFiles(_ruta, "*.jpg");
                FileInfo archivoAnterior;

                if (obtenerArchivos.Length != 0)
                {
                    archivoAnterior = new FileInfo(obtenerArchivos[0]);
                    if (archivoAnterior.Exists)
                    {
                        archivoAnterior.Delete();
                    }
                }
            }
        }
        private void Eliminar()
        {
            var NombreEscuela = ds.Tables[0].Rows[0]["RFC"].ToString();
            _escuelaManejador.Eliminar(Convert.ToString(NombreEscuela));
        }
        private void BtnBuscarJPG_Click(object sender, EventArgs e)
        {
            CargarArchivo();
            //MostrarImagen();
        }

        private void BtnEliminar_Click(object sender, EventArgs e)
        {
            if (MessageBox.Show("Desea Eliminar el Registro?", "Eliminar Registro", MessageBoxButtons.YesNo) == DialogResult.Yes)
            {
                try
                {
                    Eliminar();
                    EliminarJGP();
                    LimpiarCuadros();
                    Mostrar();
                    btnNuevo.Enabled = true;
                }
                catch (Exception ex)
                {

                    MessageBox.Show(ex.Message);
                }

            }
            

        }
        private void Mostrar()
        {
            ds = _escuelaManejador.Mostrar();
            try
            {
                txtNombreEscuela.Text = ds.Tables[0].Rows[0]["NombreEscuela"].ToString();
                txtRFC.Text = ds.Tables[0].Rows[0]["RFC"].ToString();
                txtDomicilio.Text = ds.Tables[0].Rows[0]["Domicilio"].ToString();
                txtNumeroTelefono.Text = ds.Tables[0].Rows[0]["NumeroTelefono"].ToString();
                txtCorreoE.Text = ds.Tables[0].Rows[0]["CorreroE"].ToString();
                txtPaginaWeb.Text = ds.Tables[0].Rows[0]["PaginaWeb"].ToString();
                txtDirectorEscuela.Text = ds.Tables[0].Rows[0]["NombreDirector"].ToString();
                lblImagen.Text = ds.Tables[0].Rows[0]["LogoEscuela"].ToString();
            }
            catch (Exception ex)
            {

                MessageBox.Show(ex.Message);
            }
        }
    }
}
