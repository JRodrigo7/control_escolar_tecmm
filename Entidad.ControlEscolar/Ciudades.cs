﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Ciudades
    {
        private int _CodigoCiudad;
        private string _NombreC;
        private string _fkEstado;

        public int CodigoCiudad { get => _CodigoCiudad; set => _CodigoCiudad = value; }
        public string NombreC { get => _NombreC; set => _NombreC = value; }
        public string fkEstado { get => _fkEstado; set => _fkEstado = value; }
    }
}
