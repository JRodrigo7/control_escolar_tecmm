﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Asignacion
    {
        private int _idA;
        private string _fkNControl;
        private string _fkIDMateria;
        private string _fkClaveGrupo;

        public int idA { get => _idA; set => _idA = value; }
        public string fkNControl { get => _fkNControl; set => _fkNControl = value; }
        public string fkIDMateria { get => _fkIDMateria; set => _fkIDMateria = value; }
        public string fkClaveGrupo { get => _fkClaveGrupo; set => _fkClaveGrupo = value; }
    }
}
