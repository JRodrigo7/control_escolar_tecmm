﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Escuela
    {
        private string _NombreEscuela;
        private string _RFC;
        private string _Domicilio;
        private int _NumeroTelefono;
        private string _CorreoE;
        private string _PaginaWeb;
        private string _NombreDirector;
        private string _LogoEscuela;

        public string NombreEscuela { get => _NombreEscuela; set => _NombreEscuela = value; }
        public string RFC { get => _RFC; set => _RFC = value; }
        public string Domicilio { get => _Domicilio; set => _Domicilio = value; }
        public int NumeroTelefono { get => _NumeroTelefono; set => _NumeroTelefono = value; }
        public string CorreoE { get => _CorreoE; set => _CorreoE = value; }
        public string PaginaWeb { get => _PaginaWeb; set => _PaginaWeb = value; }
        public string NombreDirector { get => _NombreDirector; set => _NombreDirector = value; }
        public string LogoEscuela { get => _LogoEscuela; set => _LogoEscuela = value; }
    }
}
