﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Profesores
    {
        private string _NControl;
        private string _Nombre;
        private string _ApellidoP;
        private string _ApellidoM;
        private string _Direccion;
        private string _Estado;
        private string _Ciudad;
        private int _NCedula;
        private string _Titulo;
        private string _FNacimiento;

        public string NControl { get => _NControl; set => _NControl = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string ApellidoP { get => _ApellidoP; set => _ApellidoP = value; }
        public string ApellidoM { get => _ApellidoM; set => _ApellidoM = value; }
        public string Direccion { get => _Direccion; set => _Direccion = value; }
        public string Estado { get => _Estado; set => _Estado = value; }
        public string Ciudad { get => _Ciudad; set => _Ciudad = value; }
        public int NCedula { get => _NCedula; set => _NCedula = value; }
        public string Titulo { get => _Titulo; set => _Titulo = value; }
        public string FNacimiento { get => _FNacimiento; set => _FNacimiento = value; }

    }
}
