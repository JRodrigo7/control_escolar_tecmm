﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Materia
    {
        private int _NumeroMateria;
        private string _IDMateria;
        private string _NombreMateria;
        private int _NumeroSemestre;
        private int _NumeroHoras;
        private int _NumeroPractias;
        private int _TotalCreditos;

        public int NumeroMateria { get => _NumeroMateria; set => _NumeroMateria = value; }
        public string IDMateria { get => _IDMateria; set => _IDMateria = value; }
        public string NombreMateria { get => _NombreMateria; set => _NombreMateria = value; }
        public int NumeroSemestre { get => _NumeroSemestre; set => _NumeroSemestre = value; }
        public int NumeroHoras { get => _NumeroHoras; set => _NumeroHoras = value; }
        public int NumeroPracticas { get => _NumeroPractias; set => _NumeroPractias = value; }
        public int TotalCreditos { get => _TotalCreditos; set => _TotalCreditos = value; }
    }
}
