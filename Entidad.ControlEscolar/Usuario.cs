﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Usuario
    {
        private int _idUsuario;
        private string _Nombre;
        private string _apellidoPaterno;
        private string _apellidoMaterno;
        private string _Contrasenia;

        public int IdUsuario { get => _idUsuario; set => _idUsuario = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string ApellidoPaterno { get => _apellidoPaterno; set => _apellidoPaterno = value; }
        public string ApellidoMaterno { get => _apellidoMaterno; set => _apellidoMaterno = value; }
        public string Contrasenia { get => _Contrasenia; set => _Contrasenia = value; }
    }
}