﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Alumno
    {
        private int _NumeroC;
        private string _Nombre;
        private string _ApellidoP;
        private string _ApellidoM;
        private string _FechaN;
        private string _Domicilio;
        private string _CorreoE;
        private string _Sexo;
        private string _Estado;
        private string _Ciudad;

        public int NumeroC { get => _NumeroC; set => _NumeroC = value; }
        public string Nombre { get => _Nombre; set => _Nombre = value; }
        public string ApellidoP { get => _ApellidoP; set => _ApellidoP = value; }
        public string ApellidoM { get => _ApellidoM; set => _ApellidoM = value; }
        public string FechaN { get => _FechaN; set => _FechaN = value; }
        public string Domicilio { get => _Domicilio; set => _Domicilio = value; }
        public string CorreoE { get => _CorreoE; set => _CorreoE = value; }
        public string Sexo { get => _Sexo; set => _Sexo = value; }
        public string Estado { get => _Estado; set => _Estado = value; }
        public string Ciudad { get => _Ciudad; set => _Ciudad = value; }
    }
}
