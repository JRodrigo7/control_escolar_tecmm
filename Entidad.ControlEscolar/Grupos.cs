﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Grupos
    {
        private int _idGrupo;
        private int _NumControl;
        private string _Grupo;
        private string _Semestre;
        private string _Turno;
        private string _ClaveGrupo;

        public int idGrupo { get => _idGrupo; set => _idGrupo = value; }
        public int NumControl { get => _NumControl; set => _NumControl = value; }
        public string Grupo { get => _Grupo; set => _Grupo = value; }
        public string Semestre { get => _Semestre; set => _Semestre = value; }
        public string Turno { get => _Turno; set => _Turno = value; }
        public string ClaveGrupo { get => _ClaveGrupo; set => _ClaveGrupo = value; }
    }
}
