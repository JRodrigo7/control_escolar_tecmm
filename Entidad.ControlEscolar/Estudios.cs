﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Estudios
    {
        private int _NEstudios;
        private string _Estudio;
        private string _Documento;
        private string _fkProfesor;

        public int NEstudios { get => _NEstudios; set => _NEstudios = value; }
        public string Estudio { get => _Estudio; set => _Estudio = value; }
        public string Documentos { get => _Documento; set => _Documento = value; }
        public string fkProfesor { get => _fkProfesor; set => _fkProfesor = value; }
    }
}
