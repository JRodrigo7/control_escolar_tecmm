﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Entidad.ControlEscolar
{
    public class Calificaciones
    {
        private int _idC;
        private string _fkClaveGrupo;
        private int _fkNumeroControl;
        private string _fkIDMateria;
        private int _Parcial_1;
        private int _Parcial_2;
        private int _Parcial_3;
        private int _Parcial_4;

        public int idC { get => _idC; set => _idC = value; }
        public string fkClaveGrupo { get => _fkClaveGrupo; set => _fkClaveGrupo = value; }
        public int fkNumeroControl { get => _fkNumeroControl; set => _fkNumeroControl = value; }
        public string fkIDMateria { get => _fkIDMateria; set => _fkIDMateria = value; }
        public int Parcial_1 { get => _Parcial_1; set => _Parcial_1 = value; }
        public int Parcial_2 { get => _Parcial_2; set => _Parcial_2 = value; }
        public int Parcial_3 { get => _Parcial_3; set => _Parcial_3 = value; }
        public int Parcial_4 { get => _Parcial_4; set => _Parcial_4 = value; }
    }
}
