﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class AsignacionManejador
    {
        private AsigancionAccesoDatos _asigancionAccesoDatos = new AsigancionAccesoDatos();

        public void Guardar(Asignacion asignacion)
        {
            _asigancionAccesoDatos.Guardar(asignacion);
        }
        public void Eliminar(int idA)
        {
            _asigancionAccesoDatos.Eliminar(idA);
        }
        public List<Asignacion> GetAsignacions(string filtro)
        {
            var listAsignacion = _asigancionAccesoDatos.GetAsignacions(filtro);
            return listAsignacion;
        }
    }
}
