﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class MateriaManejador
    {
        private MateriaAccesoDatos _materiaAccesoDatos = new MateriaAccesoDatos();
        public void Guardar(Materia materia)
        {
            _materiaAccesoDatos.Guardar(materia);
        }
        public void Eliminar(int NumeroM)
        {
            _materiaAccesoDatos.Eliminar(NumeroM);
        }
        public List<Materia> GetMaterias(string filtro)
        {
            var listMaterias = _materiaAccesoDatos.GetMaterias(filtro);
            return listMaterias;
        }
    }
}
