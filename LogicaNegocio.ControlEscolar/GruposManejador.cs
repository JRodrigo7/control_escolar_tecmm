﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class GruposManejador
    {
        private GrupoAccesoDatos _gruposManejador = new GrupoAccesoDatos();

        public void Guardar(Grupos grupos)
        {
            _gruposManejador.Guardar(grupos);
        }
        public void Eliminar(int idGrupo)
        {
            _gruposManejador.Eliminar(idGrupo);
        }
        public List<Grupos> GetGrupos(string filtro)
        {
            var listGrupos = _gruposManejador.GetGrupos(filtro);
            return listGrupos;
        }
    }
}
