﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class ProfesorManejador
    {
        private ProfesorAccesoDatos _profesorAccesoDatos = new ProfesorAccesoDatos();

        public void Guardar(Profesores profesores)
        {
            _profesorAccesoDatos.Guardar(profesores);
        }
        public void Eliminar(string NControl)
        {
            _profesorAccesoDatos.Eliminar(NControl);
        }
        public List<Profesores> GetProfesors(string filtro)
        {
            var listProfesores = _profesorAccesoDatos.GetProfesors(filtro);
            return listProfesores;
        }
    }
}
