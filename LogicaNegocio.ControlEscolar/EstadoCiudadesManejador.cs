﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class EstadoCiudadesManejador
    {
        private EstadoCiudadesAccesoDatos _estadoCiudadesAccesoDatos = new EstadoCiudadesAccesoDatos();

        public List<Estados> GetEstados(string filtro)
        {
            var listEstados = _estadoCiudadesAccesoDatos.GetEstados(filtro);
            return listEstados;
        }
        public List<Ciudades> GetCiudades(string filtro)
        {
            var listCiudades = _estadoCiudadesAccesoDatos.GetCiudades(filtro);
            return listCiudades;
        }
        public void Actualizar(string nombre)
        {
            _estadoCiudadesAccesoDatos.Actualizar(nombre);
         
        }
        public List<Estados> GetCodigos(string filtro)
        {
            var listCodigo = _estadoCiudadesAccesoDatos.GetCodigos(filtro);
            return listCodigo;
        }
    }
}
