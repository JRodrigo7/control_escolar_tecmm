﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class CalificacionesManejador
    {
        private CalificacionesAccesoDatos _calificacionesAccesoDatos = new CalificacionesAccesoDatos();
      
        public void Guardar(Calificaciones calificaciones)
        {
            _calificacionesAccesoDatos.Guardar(calificaciones);
        }
        public void Eliminar(int idC)
        {
            _calificacionesAccesoDatos.Eliminar(idC);
        }
        public List<Calificaciones> GetCalificaciones(string filtro)
        {
            var listCalificaciones = _calificacionesAccesoDatos.GetCalificaciones(filtro);
            return listCalificaciones;
        }
    }
}
