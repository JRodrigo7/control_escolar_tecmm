﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class EstudiosManejador
    {
        private EstudiosAccesoDatos _estudiosAccesoDatos = new EstudiosAccesoDatos();

        public void Guardar(Estudios estudios)
        {
            _estudiosAccesoDatos.Guardar(estudios);
        }
        public void Eliminar(int Nestudios)
        {
            _estudiosAccesoDatos.Eliminar(Nestudios);
        }
        public List<Estudios> GetEstudios(string filtro)
        {
            var ListEstudios = _estudiosAccesoDatos.GetEstudios(filtro);
            return ListEstudios;
        }
        public List<Profesores> GetProfesores(string filtro)
        {
            var listProfesores = _estudiosAccesoDatos.GetProfesores(filtro);
            return listProfesores;
        }
    }
}
