﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Data;

namespace LogicaNegocio.ControlEscolar
{
    public class EscuelaManejador
    {
        private EscuelaAccesoDatos _escuelaAccesoDatos = new EscuelaAccesoDatos();

        public void Guardar(Escuela escuela)
        {
            _escuelaAccesoDatos.Guardar(escuela);
        }
        public void Modificar(Escuela escuela)
        {
            _escuelaAccesoDatos.Modificar(escuela);
        }
        public void Eliminar(string RFC)
        {
            _escuelaAccesoDatos.Eliminar(RFC);
        }
        public List<Escuela> GetEscuelas(string filtro)
        {
            var listEscuela = _escuelaAccesoDatos.GetEscuelas(filtro);
            return listEscuela;

        }
        public DataSet Mostrar()
        {
            DataSet ds = _escuelaAccesoDatos.Mostrar();
            return ds;
        }
    }
}
