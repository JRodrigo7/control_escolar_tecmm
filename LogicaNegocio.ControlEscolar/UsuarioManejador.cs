﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;
using System.Text.RegularExpressions;

namespace LogicaNegocio.ControlEscolar
{
    public class UsuarioManejador
    {
        private UsuarioAccesoDatos _usuarioaccesoDatos = new UsuarioAccesoDatos();

        private bool NombreValido(string nombre)
        {
            var regex = new Regex(@"^[A-Za-z]+( [A-Za-z]+)*$");
            var match = regex.Match(nombre);

            if (match.Success)
            {
                return true;
            }
            return false;
        }
        public Tuple<bool, string> ValidarUsuario(Usuario usuario)
        {
            string mensaje = "";
            bool valido = true;

            if (usuario.Nombre.Length==0)
            {
                mensaje = mensaje + "El nombre de usuario es necesario \n";
                valido = false;
            }
            else if (usuario.Nombre.Length>20)
            {
                mensaje = mensaje + "El nombre de usuario solo permite un maximo de 20 caracteres";
                valido = false;
            }
            else if (!NombreValido(usuario.Nombre))
            {
                mensaje = mensaje + "Escribe un formato valido para el nombre del usuario";
                valido = false;
            }
            if (usuario.ApellidoPaterno.Length==0)
            {
                mensaje = mensaje + "El Apellido Paterno en necesario para el usuario";
                valido = false;
            }
            else if (usuario.ApellidoPaterno.Length>20)
            {
                mensaje = mensaje + "El Apellido Paterno solo permite 20 caracteres";
                valido = false;
            }

            return Tuple.Create(valido, mensaje);
        }

        public void Guardar(Usuario usuario)
        {
            _usuarioaccesoDatos.Guardar(usuario);
        }
        public void Eliminar(int idUsuario)
        {
            _usuarioaccesoDatos.Eliminar(idUsuario);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            var listUsuario = _usuarioaccesoDatos.GetUsuarios(filtro);
            return listUsuario;
        }
    }
}
