﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;
using AccesoDatos.ControlEscolar;

namespace LogicaNegocio.ControlEscolar
{
    public class AlumnoManejador
    {
        private AlumnoAccesoDatos _alumnoaccesoDatos = new AlumnoAccesoDatos();


        public void Guardar(Alumno alumno)
        {
            _alumnoaccesoDatos.Guardar(alumno);
        }
        public void Eliminar(int NumeroC)
        {
            _alumnoaccesoDatos.Eliminar(NumeroC);
        }
        public List<Alumno> GetAlumnos(string filtro)
        {
            var listAlumno = _alumnoaccesoDatos.GetAlumnos(filtro);
            return listAlumno;
        }
    }
}
