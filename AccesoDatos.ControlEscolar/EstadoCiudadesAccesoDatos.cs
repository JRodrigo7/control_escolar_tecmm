﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class EstadoCiudadesAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public EstadoCiudadesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public List<Estados> GetEstados(string filtro)
        {
            var ListEstado = new List<Estados>();
            var ds = new DataSet();
            string consulta = "select * from estados where nombre like '%"+filtro+"%'";
            ds = conexion.ObtenerDatos(consulta, "estados");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var estados = new Estados
                {
                    Codigo = row["codigo"].ToString(),
                    Nombre = row["nombre"].ToString()
                };
                ListEstado.Add(estados);

            }
            return ListEstado;
        }
        public List<Ciudades> GetCiudades(string filtro)
        {
            var ListCiudad = new List<Ciudades>();
            var ds = new DataSet();
            string consulta = "select * from ciudades where fkEstado like '%"+filtro+"%';";
            ds = conexion.ObtenerDatos(consulta, "ciudades");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var ciudades = new Ciudades
                {
                    CodigoCiudad = Convert.ToInt32(row["CodigoCiudad"]),
                    NombreC = row["NombreC"].ToString(),
                    fkEstado = row["fkEstado"].ToString()
                };
                ListCiudad.Add(ciudades);

            }
            return ListCiudad;
        }
        public string Actualizar(string nombre)
        {
            string consulta = string.Format("select codigo from estados where nombre like '%" + nombre + "%';");
            conexion.EjecutarConsulta(consulta);
            return consulta;
        }
        public List<Estados> GetCodigos(string filtro)
        {
            var ListCodigo = new List<Estados>();
            var ds = new DataSet();
            string consulta = "select * from estados where nombre like '%"+filtro+"%'; ";
            ds = conexion.ObtenerDatos(consulta, "estados");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var codigos = new Estados
                {
                    Codigo = row["codigo"].ToString(),
                    Nombre = row["nombre"].ToString()
                };
                ListCodigo.Add(codigos);

            }
            return ListCodigo;
        }
    }
}
