﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class MateriaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public MateriaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Materia materia)
        {
            if(materia.NumeroMateria==0)
            {
                string consulta = string.Format("Insert into materia values (null,'{0}','{1}','{2}','{3}')", materia.IDMateria, materia.NombreMateria, materia.NumeroHoras, materia.NumeroPracticas);
            }
            else
            {
                string consulta = string.Format("Update materia set IDMateria='{0}',NombreMateria='{1}',NumeroHoras='{2}', NumeroPracticas='{3}' where NumeroMateria='{4}'", materia.IDMateria, materia.NombreMateria, materia.NumeroHoras, materia.NumeroPracticas, materia.NumeroMateria);
            }
        }
        public void Eliminar(int NumeroM)
        {
            string consulta = string.Format("Delete from materia where NumeroMateria='{0}'", NumeroM);
            conexion.EjecutarConsulta(consulta);

        }
        public List<Materia> GetMaterias(string filtro)
        {
            var listMaterias = new List<Materia>();
            var ds = new DataSet();
            string consulta = "Select * from materia where NombreMateria like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "materia");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var materia = new Materia
                {
                    NumeroMateria = Convert.ToInt32(row["NumeroMateria"]),
                    IDMateria = row["IDMateria"].ToString(),
                    NombreMateria = row["NombreMateria"].ToString(),
                    NumeroHoras = Convert.ToInt32(row["NumeroHoras"]),
                    NumeroPracticas = Convert.ToInt32(row["NumeroPractica"])
                };
                listMaterias.Add(materia);
            }
            return listMaterias;
        }
    }
}
