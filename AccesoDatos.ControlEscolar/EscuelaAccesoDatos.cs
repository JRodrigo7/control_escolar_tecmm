﻿
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class EscuelaAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EscuelaAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Escuela escuela)
        {
            string consulta = string.Format("insert into escuela values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}')", escuela.NombreEscuela, escuela.RFC,escuela.Domicilio, escuela.NumeroTelefono, escuela.CorreoE, escuela.PaginaWeb, escuela.NombreDirector, escuela.LogoEscuela);
            conexion.EjecutarConsulta(consulta);
           
        }
        public void Modificar(Escuela escuela)
        {
            string consuta = string.Format("update escuela set RFC='{0}',Domicilio='{1}',NumeroTelefono='{2}',CorreroE='{3}',PaginaWeb='{4}',NombreDirector='{5}',LogoEscuela='{6}' where NombreEscuela='{7}'", escuela.RFC, escuela.Domicilio, escuela.NumeroTelefono, escuela.CorreoE, escuela.PaginaWeb, escuela.NombreDirector, escuela.LogoEscuela, escuela.NombreEscuela);
            conexion.EjecutarConsulta(consuta);
        }
        public void Eliminar(string RFC)
        {
            string consulta = string.Format("Delete from escuela where RFC='{0}'", RFC);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Escuela> GetEscuelas(string filtro)
        {
            var listEscuela = new List<Escuela>();
            string consulta = "select * from escuela";
            var ds = new DataSet();
            ds = conexion.ObtenerDatos(consulta, "escuela");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach(DataRow row in dt.Rows)
            {
                var escuela = new Escuela
                {
                    NombreEscuela = row["NombreEscuela"].ToString(),
                    RFC = row["RFC"].ToString(),
                    Domicilio = row["Domicilio"].ToString(),
                    NumeroTelefono = Convert.ToInt32(row["NumeroTelefono"]),
                    CorreoE = row["CorreroE"].ToString(),
                    PaginaWeb = row["PaginaWeb"].ToString(),
                    NombreDirector = row["NombreDirector"].ToString(),
                    LogoEscuela = row["LogoEscuela"].ToString()
                };
                listEscuela.Add(escuela);
            }
            return listEscuela;
        }
        public DataSet Mostrar()
        {
            DataSet ds;
            string consulta = "select * from escuela";
            ds = conexion.ObtenerDatos(consulta,"escuela");
            return ds;
        }

    }
}
