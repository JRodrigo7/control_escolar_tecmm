﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class ProfesorAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public ProfesorAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Profesores profesores)
        {
            if (profesores.NControl == "")
            {
                string consulta = string.Format("Insert into profesores values('{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}','{9}')", profesores.NControl, profesores.Nombre,
                profesores.ApellidoP, profesores.ApellidoM, profesores.Direccion, profesores.Estado, profesores.Ciudad, profesores.NCedula, profesores.Titulo, profesores.FNacimiento);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update profesores set Nombre ='{0}',ApellidoP='{1}', ApellidoM='{2}', Direccion='{3}',Estado='{4}',Ciudad='{5}', Ncedula='{6}', Titulo='{7}',FNacimiento='{8}' where NControl='{9}'", profesores.Nombre,
                profesores.ApellidoP, profesores.ApellidoM, profesores.Direccion, profesores.Estado, profesores.Ciudad, profesores.NCedula, profesores.Titulo, profesores.FNacimiento, profesores.NControl);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(string NControl)
        {
            string consulta = string.Format("Delete from profesores where NControl = '{0}'", NControl);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Profesores> GetProfesors(string filtro)
        {
            var listProfesores = new List<Profesores>();
            string consulta = "Select * from profesores where Nombre like '%" + filtro + "%';";
            var ds = new DataSet();
            ds = conexion.ObtenerDatos(consulta, "profesores");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var profesor = new Profesores
                {
                    NControl = row["NControl"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoP = row["ApellidoP"].ToString(),
                    ApellidoM = row["ApellidoM"].ToString(),
                    Direccion = row["Direccion"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Ciudad = row["Ciudad"].ToString(),
                    NCedula = Convert.ToInt32(row["NCedula"]),
                    Titulo = row["Titulo"].ToString(),
                    FNacimiento = row["FNacimiento"].ToString()
                };
                listProfesores.Add(profesor);
            }
            return listProfesores;
        }
    }
}
