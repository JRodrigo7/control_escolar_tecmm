﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class CalificacionesAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public CalificacionesAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Calificaciones calificaciones)
        {
            if (calificaciones.idC==0)
            {
                string consulta = string.Format("Insert into Calificaciones values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}')", calificaciones.fkClaveGrupo, calificaciones.fkNumeroControl, calificaciones.fkIDMateria, calificaciones.Parcial_1,calificaciones.Parcial_2,calificaciones.Parcial_3,calificaciones.Parcial_4);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update Calificaciones set fkClaveGrupo='{0}', fkNumeroControl='{1}', fkIDMateria='{2}', Parcial_1='{3}', Parcial_2='{4}', Parcial_3='{5}', Parcial_4='{6}' where idC='{7}'", calificaciones.fkClaveGrupo, calificaciones.fkNumeroControl, calificaciones.fkIDMateria, calificaciones.Parcial_1, calificaciones.Parcial_2, calificaciones.Parcial_3, calificaciones.Parcial_4, calificaciones.idC);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idC)
        {
            string consulta = string.Format("Delete from Calificaciones where idC ={0}", idC);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Calificaciones> GetCalificaciones(string filtro)
        {
            var listCalificaciones = new List<Calificaciones>();
            var ds = new DataSet();
            string consulta = "select * from Calificaciones where fkNumeroControl like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Calificaciones");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var calificaciones = new Calificaciones
                {
                    idC = Convert.ToInt32(row["idC"]),
                    fkClaveGrupo = row["fkClaveGrupo"].ToString(),
                    fkNumeroControl = Convert.ToInt32(row["fkNumeroControl"]),
                    fkIDMateria = row["fkIDMateria"].ToString(),
                    Parcial_1 = Convert.ToInt32(row["Parcial_1"]),
                    Parcial_2 = Convert.ToInt32(row["Parcial_2"]),
                    Parcial_3 = Convert.ToInt32(row["Parcial_3"]),
                    Parcial_4 = Convert.ToInt32(row["Parcial_4"])

                };
                listCalificaciones.Add(calificaciones);
            }
            return listCalificaciones;
        }
    }
}
