﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class EstudiosAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public EstudiosAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Estudios estudios)
        {
            if (estudios.NEstudios == 0)
            {
                string consulta = string.Format("insert into estudios values(null,'{0}','{1}','{2}');", estudios.Estudio, estudios.Documentos, estudios.fkProfesor);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update estudios set Estudio='{0}', Documento='{1}', fkProfesor='{2}' where NEstudios='{3}'", estudios.Estudio, estudios.Documentos, estudios.fkProfesor, estudios.NEstudios);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int Nestudios)
        {
            string consulta = string.Format("Delete from estudios where NEstudios={0}", Nestudios);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Estudios> GetEstudios(string filtro)
        {
            var listEstudios = new List<Estudios>();
            string consulta = "select * from estudios where Estudio like '%" + filtro + "%'";
            var ds = new DataSet();
            ds = conexion.ObtenerDatos(consulta, "estudios");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var estudios = new Estudios
                {
                    NEstudios = Convert.ToInt32(row["NEstudios"]),
                    Estudio = row["Estudio"].ToString(),
                    Documentos = row["Documento"].ToString(),
                    fkProfesor = row["fkProfesor"].ToString()
                };
                listEstudios.Add(estudios);
            }
            return listEstudios;
        }
        public List<Profesores> GetProfesores(string filtro)
        {
            var listProfesores = new List<Profesores>();
            string consulta = "select * from profesores where Ncontrol like '%D" +filtro+ "%'";
            var ds = new DataSet();
            ds = conexion.ObtenerDatos(consulta, "profesores");
            var dt = new DataTable();
            dt = ds.Tables[0];
            foreach (DataRow row in dt.Rows)
            {
                var profesor = new Profesores
                {
                    NControl = row["NControl"].ToString(),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoP = row["ApellidoP"].ToString(),
                    ApellidoM = row["ApellidoM"].ToString(),
                    Direccion = row["Direccion"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Ciudad = row["Ciudad"].ToString(),
                    NCedula = Convert.ToInt32(row["NCedula"]),
                    Titulo = row["Titulo"].ToString(),
                    FNacimiento = row["FNacimiento"].ToString()
                };
                listProfesores.Add(profesor);
            }
            return listProfesores;
        }
    }
}
