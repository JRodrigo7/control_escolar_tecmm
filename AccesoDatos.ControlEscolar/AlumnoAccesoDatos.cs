﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class AlumnoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public AlumnoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Alumno alumno)
        {
            if (alumno.NumeroC == 0)
            {
                string consulta = string.Format("Insert into alumnos values(null,'{0}','{1}','{2}','{3}','{4}','{5}','{6}','{7}','{8}')", alumno.Nombre, alumno.ApellidoP, alumno.ApellidoM, alumno.FechaN, alumno.Domicilio, alumno.CorreoE, alumno.Sexo, alumno.Estado, alumno.Ciudad);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update alumnos set Nombre ='{0}',ApellidoP='{1}', ApellidoM='{2}', FechaN='{3}', Domicilio='{4}', CorreoE='{5}', Sexo='{6}', Estado='{7}', Ciudad='{8}' where NumeroControl='{9}'", alumno.Nombre, alumno.ApellidoP, alumno.ApellidoM, alumno.FechaN, alumno.Domicilio, alumno.CorreoE, alumno.Sexo, alumno.Estado, alumno.Ciudad , alumno.NumeroC);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int NumeroC)
        {
            string consulta = string.Format("Delete from alumnos where NumeroControl={0}", NumeroC);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Alumno> GetAlumnos (string filtro)
        {
            var listAlumno = new List<Alumno>();
            var ds = new DataSet();
            string consulta = "Select * from alumnos where nombre like '%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "alumnos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var alumno = new Alumno
                {
                    NumeroC = Convert.ToInt32(row["NumeroControl"]),
                    Nombre = row["Nombre"].ToString(),
                    ApellidoP = row["ApellidoP"].ToString(),
                    ApellidoM = row["ApellidoM"].ToString(),
                    FechaN = row["FechaN"].ToString(),
                    Domicilio = row["Domicilio"].ToString(),
                    CorreoE = row["CorreoE"].ToString(),
                    Sexo = row["Sexo"].ToString(),
                    Estado = row["Estado"].ToString(),
                    Ciudad = row["Ciudad"].ToString()
                };
                listAlumno.Add(alumno);
            }
            return listAlumno;
        }
        public DataSet Ncontrol()
        {
            DataSet ds;
            string consulta = "select * from alumnos";
            ds = conexion.ObtenerDatos(consulta, "escuela");
            return ds;
        }
    }
}
