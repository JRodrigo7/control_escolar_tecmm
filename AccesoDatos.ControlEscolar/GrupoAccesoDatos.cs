﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class GrupoAccesoDatos
    {
        ConexionAccesoDatos conexion;

        public GrupoAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }

        public void Guardar(Grupos grupos)
        {
            if (grupos.idGrupo==0)
            {
                string consulta = string.Format("Insert into grupos values(null,'{0}','{1}','{2}','{3}','{4}')", grupos.NumControl, grupos.Grupo, grupos.Semestre, grupos.Turno, grupos.ClaveGrupo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update grupos set fkNumeroControl='{0}', Grupo='{1}',Semestre ='{2}',Turno='{3}',ClaveGrupo='{4}' where idGrupo='{5}'", grupos.NumControl, grupos.Grupo, grupos.Semestre, grupos.Turno, grupos.ClaveGrupo, grupos.idGrupo);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idGrupo)
        {
            string consulta = string.Format("Delete from grupos where idGrupo={0}", idGrupo);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Grupos> GetGrupos (string filtro)
        {
            var listGrupo = new List<Grupos>();
            var ds = new DataSet();
            string consulta = "Select * from grupos where ClaveGrupo like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "grupos");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var grupo = new Grupos
                {
                    idGrupo = Convert.ToInt32(row["idGrupo"]),
                    NumControl=Convert.ToInt32(row["fkNumeroControl"]),
                    Grupo = row["Grupo"].ToString(),
                    Semestre = row["Semestre"].ToString(),
                    Turno = row["Turno"].ToString(),
                    ClaveGrupo = row["ClaveGrupo"].ToString()
                };
                listGrupo.Add(grupo);
            }
            return listGrupo;
        }
        public List<Grupos> Vista(string filtro)
        {
            var listGrupos = new List<Grupos>();
            var ds = new DataSet();
            string consulta = "select * from v_Grupos";
            ds = conexion.ObtenerDatos(consulta, "v_Grupos");
            var dt = new DataTable();
            dt = ds.Tables[0];
            return listGrupos;
        }
    }
}
