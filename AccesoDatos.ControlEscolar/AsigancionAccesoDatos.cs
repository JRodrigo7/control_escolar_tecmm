﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class AsigancionAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public AsigancionAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Asignacion asignacion)
        {
            if (asignacion.idA==0)
            {
                string consulta = string.Format("Insert into Asignacion values(null,'{0}','{1}','{2}')", asignacion.fkNControl, asignacion.fkIDMateria, asignacion.fkClaveGrupo);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update Asignacion set fkNControl='{0}',fkIDMateria='{1}',fkClaveGrupo='{2}' where idA='{3}'", asignacion.fkNControl, asignacion.fkIDMateria, asignacion.fkClaveGrupo, asignacion.idA);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar(int idA)
        {
            string consulta = string.Format("Delete from Asignacion where idA ={0}", idA);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Asignacion> GetAsignacions(string filtro)
        {
            var ListAsignacion = new List<Asignacion>();
            var ds = new DataSet();
            string consulta = "Select * from Asignacion where fkNControl like'%" + filtro + "%'";
            ds = conexion.ObtenerDatos(consulta, "Asignacion");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach (DataRow row in dt.Rows)
            {
                var asignacion = new Asignacion
                {
                    idA = Convert.ToInt32(row["idA"]),
                    fkNControl = row["fkNControl"].ToString(),
                    fkIDMateria = row["fkIDMateria"].ToString(),
                    fkClaveGrupo = row["fkClaveGrupo"].ToString()
                };
                ListAsignacion.Add(asignacion);
            }
            return ListAsignacion;
        }
    }
}
