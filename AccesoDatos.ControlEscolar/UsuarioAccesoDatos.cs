﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Entidad.ControlEscolar;

namespace AccesoDatos.ControlEscolar
{
    public class UsuarioAccesoDatos
    {
        ConexionAccesoDatos conexion;
        public UsuarioAccesoDatos()
        {
            conexion = new ConexionAccesoDatos("localhost", "root", "", "empresa", 3306);
        }
        public void Guardar(Usuario usuario)
        {
            if (usuario.IdUsuario==0)
            {
                string consulta = string.Format("Insert into usuario values(null,'{0}','{1}','{2}','{3}')", usuario.Nombre, usuario.ApellidoPaterno, usuario.ApellidoMaterno, usuario.Contrasenia);
                conexion.EjecutarConsulta(consulta);
            }
            else
            {
                string consulta = string.Format("Update usuario set nombre ='{0}',apellidopaterno='{1}', apellidomaterno='{2}', contrasenia='{3}' where idusuario='{4}'", usuario.Nombre, usuario.ApellidoPaterno, usuario.ApellidoMaterno, usuario.Contrasenia, usuario.IdUsuario);
                conexion.EjecutarConsulta(consulta);
            }
        }
        public void Eliminar( int idUsuario)
        {
            string consulta = string.Format("Delete from usuario where idusuario={0}", idUsuario);
            conexion.EjecutarConsulta(consulta);
        }
        public List<Usuario> GetUsuarios(string filtro)
        {
            //List<Usuario> listUsuario = new List<Usuario>();
            var listUsuario = new List<Usuario>();
            var ds = new DataSet();
            string consulta = "Select * from usuario where nombre like '%"+filtro+"%'";
            ds = conexion.ObtenerDatos(consulta,"usuario");
            var dt = new DataTable();
            dt = ds.Tables[0];

            foreach  (DataRow row in dt.Rows)
            {
                var usuario = new Usuario
                {
                    IdUsuario=Convert.ToInt32(row["idusuario"]),
                    Nombre=row["nombre"].ToString(),
                    ApellidoPaterno= row["apellidopaterno"].ToString(),
                    ApellidoMaterno= row["apellidomaterno"].ToString(),
                    Contrasenia=row["contrasenia"].ToString()
                };
                listUsuario.Add(usuario);
            }

            //HardCodear
            /*listUsuario.Add(new Usuario

            {
                IdUsuario = row["idUsuario"],
                Nombre = row["nombre"],
                ApellidoPaterno = "Chico",
                ApellidoMaterno = "Rojas",
                Contrasenia = "Tacos123"
            }
                );

            listUsuario.Add(new Usuario

            {
                IdUsuario = 2,
                Nombre = "Parker",
                ApellidoPaterno = "Alfonso",
                ApellidoMaterno = "Gutierritos",
                Contrasenia ="Patas21"
            }
               );*/
            //Llenar lista
            return listUsuario;
        }
    }
}
